#include<iostream>
using namespace std;

int main()
{
	cout<<"This will print the size of Variables of different Types"<<endl;
	char ch = 'a';
	unsigned char uCh = 'A';
	signed char sCh ;
	int i;
	unsigned int uI;
	signed int sI;
	short int shI;
	unsigned short int uShI;
	signed short int sShI;
	long int lI;
	signed long int slI;
	unsigned long int ulI;
	float f;
	double d;
	long double lD;
	wchar_t wC;
	
	cout<<"char Size :         :  "<<sizeof(char)<< 		 "  bytes"<<endl;
	cout<<"unsigned char Size  :  "<<sizeof(unsigned char)<<	 "  bytes"<<endl;
	cout<<"signed char Size    :  "<<sizeof(signed char)<< 		 "  bytes"<<endl;
	cout<<"int Size            :  "<<sizeof(int)<<			 "  bytes"<<endl;
	cout<<"insigned int Size   :  "<<sizeof(unsigned int)<<		 "  bytes"<<endl;
	cout<<"signed int          :  "<<sizeof(signed int)<<		 "  bytes"<<endl;
	cout<<"short int           :  "<<sizeof(short int)<<		 "  bytes"<<endl;
	cout<<"unsigned short int  :  "<<sizeof(unsigned short int)<<	 "  bytes"<<endl;
	cout<<"signed short int    :  "<<sizeof(signed short int)<<	 "  bytes"<<endl;
	cout<<"long int            :  "<<sizeof(long int)<<		 "  bytes"<<endl;
	cout<<"signed long int     :  "<<sizeof(signed long int)<<	 "  bytes"<<endl;
	cout<<"unsigned long int   :  "<<sizeof(unsigned long int)<<	 "  bytes"<<endl;
	cout<<"flot                :  "<<sizeof(float)<<		 "  bytes"<<endl;
	cout<<"double              :  "<<sizeof(double)<<		 "  bytes"<<endl;
	cout<<"long doubl          :  "<<sizeof(long double)<<		 " bytes"<<endl;
	cout<<"wchar_t             :  "<<sizeof(wchar_t)<<	 	 "  bytes"<<endl;
	
}
