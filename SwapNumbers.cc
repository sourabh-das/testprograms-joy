#include<iostream>
using namespace std;

int main()
{
	int a = 2, b = -8 ;

	cout<<"ORIGINAL   :  a = "<<a<<" :: b = "<<b<<endl;
	a = a + b;
	b = a - b;
	a = a - b;
	cout<<"AFTER SWAP :  a = "<<a<<" :: b = "<<b<<endl;
	
	return 0;
}

