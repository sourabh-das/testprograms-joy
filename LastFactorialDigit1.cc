#include<iostream>
using namespace std;

int factorial(int n);

int main()
{
    int n,p;
    cin >> n;

    for(int i = 0;i<n;i++)
    {
	cin>>p;
    	cout << factorial(p)%10 <<endl;
    }

    return 0;
}

int factorial(int n)
{
    if(n > 1)
        return n * factorial(n - 1);
    else
        return 1;
}

